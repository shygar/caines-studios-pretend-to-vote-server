//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//


var uuidG = require('node-uuid');


var http = require('http');
var path = require('path');

var async = require('async');
var socketio = require('socket.io');
var express = require('express');

// Start redis with redis-server in a console
var redis = require("redis"),
    rClient = redis.createClient("redis://127.0.0.1:6379");
    
    
//
// ## SimpleServer `SimpleServer(obj)`
//
// Creates a new instance of SimpleServer with the following options:
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
//
var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

var test = true

var votingChoices = ["not_voting","hillary_clinton","gary_johnson","jill_stein","donald_trump","other"]


router.get('/verify/:uuid', function(req, res) {
   
  uuidMap[req.params.uuid].socket.emit("verifiedUUID", req.params.uuid);
});

rClient.on("error", function(err) {
    console.log("Error " + err);
});





router.use(express.static(path.resolve(__dirname, 'client')));
var sockets = [];


var uuidMap = {}

io.on('connection', function(socket) {

  sockets.push(socket);

  socket.on('disconnect', function() {
    //sockets.splice(sockets.indexOf(socket), 1);
    //updateRoster();
  });

  socket.on('phone', function(msg) {
    var phoneNum;
    if(isObject(msg)){
      phoneNum = msg.phone;
    }
    else{
      phoneNum = String(msg || '');
    }
        console.log("Phone: " + phoneNum); 

    if (!phoneNum)
      return;
      
    var accountSid = 'ACd4ce8c4d346d467ba800febadf6b59a4'; 
    var authToken = 'd1f91cd26991121484534604da613919'; 
     
    //require the Twilio module and create a REST client 
    var client = require('twilio')(accountSid, authToken); 
    
    var uuid = uuidG.v4()
    uuidMap[uuid]= {"socket":socket,"phoneNum":phoneNum}
    
    socket.emit("message", "validated");
    var body = "https://your-vote-terra1.c9users.io/verify/"+uuid 
    if(test){
      uuidMap[uuid].socket.emit("verifiedUUID", uuid);
      return;
    }
    
    client.messages.create({ 
        to: "+1"+phoneNum, 
        from: "+17039626993", 
        body: body
    }, function(err, message) { 
        console.log(err)
        console.log(message); 
    });  
      
    


  });
  
  socket.on('votingInfo', function(data) {
    //verify the phone number and uuid match
    if(!uuidMap[data.uuid]){
      return
    }
    if(data.phoneNum != uuidMap[data.uuid].phoneNum){
      return
    }
    if(!data.votingChoice in votingChoices){
      return
    }
        
    //see if the phone number exists in a hash set.  If it does then replace it and alter the increments as appropriate
    
    rClient.hget("voteSet",data.phoneNum,function(err, dataPrev) {
      if(dataPrev){
        //verify valid voting choice
        rClient.decr("voteChoice-"+JSON.parse(dataPrev).votingChoice,function(err, data) {});
        
      }
      rClient.incr("voteChoice-"+data.votingChoice,function(err, data) {
        
      });
      rClient.hset("voteSet",data.phoneNum,JSON.stringify(data),function(err, data) {
        sendVotes();
      });
      
      console.log(data);
      
    }); 
  });
  
  function sendVotes(){
    rClient.multi()
      .get("voteChoice-not_voting")
      .get("voteChoice-hillary_clinton")
      .get("voteChoice-gary_johnson")
      .get("voteChoice-jill_stein")
      .get("voteChoice-donald_trump")
      .get("voteChoice-other")
      .exec(function (err, replies) {
          var votes = []
          console.log("MULTI got " + replies.length + " replies");
          replies.forEach(function (reply, index) {
              console.log("Reply " + index + ": " + reply);
              if(reply == null){
                reply = 0;
              }
              votes.push(reply)
          });
          socket.emit("currentVotes", votes);
      });
  }
  sendVotes()

});

function isObject(obj) {
  return obj === Object(obj);
}

server.listen(process.env.PORT || 80, process.env.IP || "0.0.0.0", function() {
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});



